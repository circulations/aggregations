# aggregations

## run

`$ python3 aggregator.py`

`$ python3 -m http.server`

<localhost:8000>

## install

`$ python3 -m venv venv`

`$ source venv/bin/activate`

`$ pip install -r requirements.txt`

## optional pad API function

`$ cp pad_settings.py.example pad_settings.py`

`then edit the pad_settings.py according to the pad API you have access to.`
